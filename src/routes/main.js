const { response } = require('express')
const express = require('express')
const {route} = require('express/lib/application')
const Contact = require('../models/Contact')

const Detail = require("../models/Detail")
const Service = require('../models/Service')
const Slider = require('../models/Slider')

const routes = express.Router()
//asunk  -sink with mongo and await connect database
routes.get("/", async (req,res) => {
    const details= await Detail.findOne({"_id":"62b1c2e29eb6ebda2018b866"})
    const sliders= await Slider.find()
    const services=await Service.find()
    // console.log(sliders)

    res.render("index",{
        details:details,
        sliders:sliders,
        services:services
    })
})
routes.get("/gallery", async (req,res) => {
    const details= await Detail.findOne({"_id":"62b1c2e29eb6ebda2018b866"})
    // console.log(details)

    res.render("gallery",{
        details:details,
    })
})

routes.post("/process-contact-form", async(request,response) =>{
    console.log("form is Summit")
    // console.log(request.body)
    //Save the Data into DB
    try{
        const data=await Contact.create(request.body)
        // console.log(data)
        response.redirect("/")
    }catch{
        console.log(e)
        response.redirect("/")
    }
})



module.exports = routes;