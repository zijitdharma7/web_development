const { request, response } = require("express")
const express =require("express")
const hbs = require("hbs")
const bodyParser=require('body-parser')

const app = express();
const mongoose = require("mongoose")

const routes = require('./routes/main')
const Detail = require('./models/Detail')
const Slider = require('./models/Slider')
const Service=require("./models/Service")


app.use(bodyParser.urlencoded({
    extended:true
}))

// Access static file (Css/Js/image) 
app.use('/static',express.static("public"))

//use routing 
app.use('',routes)


// (View tamplet engine --hbs)

app.set('view engine','hbs')
app.set("views","views")
hbs.registerPartials("views/partials")

//Database Conncection 
mongoose.connect("mongodb://localhost/website_node",()=>{
    console.log("db connected")
    // Service.create([
    //     {
    //         icon:'fab fa-accusoft',
    //         title:'Provide Best Cources',
    //         description:'We Provide  Best Cources that help our Studen learn',
    //         linkText:'https://github.com/dharmajit7b',
    //         link:'Check'
    //     },
    //     {
    //         icon:'fas fa-analytics',
    //         title:'Data Science',
    //         description:'We Provide  Best Cources that help our Studen learn',
    //         linkText:'https://github.com/dharmajit7b',
    //         link:'Check'
    //     },
    //     {
    //         icon:'fas fa-laptop-code',
    //         title:'Learn Code',
    //         description:'We Provide  Best Cources that help our Studen learn',
    //         linkText:'https://github.com/dharmajit7b',
    //         link:'Check'
    //     }
    // ])

/**Slider create in mongo db */
    // Slider.create([
    //     {
    //         title:'Search Your Happyness',
    //         subTitle:'Travel for Refreshment',
    //         imageUrl:"/static/images/s1.png"
    //     },
    //     {
    //         title:'I want move with you  ',
    //         subTitle:'we have best time with fev person ',
    //         imageUrl:"/static/images/s2.png"
    //     },
    //     {
    //         title:'Sen set with promesh ',
    //         subTitle:'We will meet Again',
    //         imageUrl:"/static/images/s3.png"
    //     }
    // ])
/**Details Connect in mongo */
    // Detail.create({
    //     brandName:"InfoTech",
    //     brandIconeUrl:"/static/images/logo-search-grid-1x.png",
    //     links:[
    //         {
    //             label:"Home",
    //             url:"/"
    //         },
    //         {
    //             label:"Services",
    //             url:"/#services_section"
    //         },
    //         {
    //             label:"Gallery",
    //             url:"/gallery"
    //         },
    //         {
    //             label:"About",
    //             url:"/#about_us"
    //         },
    //         {
    //             label:"Contact",
    //             url:"/#contact_us_section"
    //         },
    //     ]
    // })
})

mongoose.con

app.listen(process.env.PORT |5556,() => {
    console.log("server started");
});